package main

import "fmt"

type address struct {
	description string
	number      int
}

type people struct {
	name     string
	lastname string
	age      int
	pAddress []*address
}

func newPeople(name, lName string) *people {
	return &people{
		name:     name,
		lastname: lName,
		age:      0,
		pAddress: make([]*address, 0),
	}
}

func newAddress(dc string, nb int) *address {
	return &address{
		description: dc,
		number:      nb,
	}
}

type product struct {
	name  string
	value float32
}

type inventory struct {
	products []*product
}

func newProduct(n string, v float32) *product {
	return &product{
		name:  n,
		value: v,
	}
}

func newInventory() *inventory {
	return &inventory{
		products: make([]*product, 0),
	}
}

func (i *inventory) AddProduct(n string, v float32) {
	i.products = append(i.products, newProduct(n, v))
}

func main() {

	client := newPeople("Obi-Wan", "Kenobi")

	client.age = 57

	client.pAddress = append(client.pAddress, newAddress("Coruscant", 333))
	client.pAddress = append(client.pAddress, newAddress("Dagobah", 666))

	fmt.Println("Cliente ")
	fmt.Println(client.name, client.lastname, "Idade:", client.age)

	fmt.Println("Endereços ")
	for _, add := range client.pAddress {
		fmt.Println(add.description, "- Numero:", add.number)
	}

	iv := newInventory()
	iv.AddProduct("caderno", 8.90)
	iv.AddProduct("lapis", 1.20)

	fmt.Println("Produtos no estoque ")
	for _, prod := range iv.products {
		fmt.Println("Produto:", prod.name, "Valor:", prod.value)
	}
}
