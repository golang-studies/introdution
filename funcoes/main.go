package main

import "fmt"

func printValue(v string) {
	fmt.Println(v)
}

func sum(n1 int, n2 int) int {
	return n1 + n2
}

func isEquals(n1, n2 int) (bool, int) {
	if n1 != n2 {
		return false, -1
	}
	return true, sum(n1, n2)
}

func main() {

	multiply := func(n1 int, n2 int) int {
		return n1 * n2
	}

	fmt.Println("2 + 5 = ", sum(2, 5))
	fmt.Println("2 x 5 = ", multiply(2, 5))

	printValue("Hello world!")

	err, r := isEquals(3, 3)

	fmt.Println("Is Equals -> ", err, " \nResult -> ", r)
}
