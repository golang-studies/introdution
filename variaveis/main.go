package main

import "fmt"

func main() {
	/* Forma Convencional de declaração de variáveis */
	var name string = "Obi-Wan"
	var age int = 57

	/* Forma abreviada de declaração de variáveis */
	lastName, address := "Kenobi", "Tatooine"
	nmbAddress := 659

	fmt.Println("full name: ", name, lastName)
	fmt.Println("age: ", age)
	fmt.Println("address: ", address, " number: ", nmbAddress)

	person1 := make(map[string]string)

	person1["name"] = "Anakim"
	person1["age"] = "27"
	person1["height"] = "170.24"

	fmt.Println(person1)

	person2 := make(map[string]interface{})
	person2["name"] = "Mando"
	person2["age"] = 32
	person2["height"] = 173.8

	fmt.Println(person2)
}
