package inventory

import "gitlab.com/golang-studies/introdution/pacotes/product"

type Inventory struct {
	Products []*product.Product
}

func NewInventory() *Inventory {
	return &Inventory{
		Products: make([]*product.Product, 0),
	}
}

func (i *Inventory) AddProduct(n string, v float32) {
	i.Products = append(i.Products, product.NewProduct(n, v))
}
