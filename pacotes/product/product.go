package product

type Product struct {
	Name  string
	Value float32
}

func NewProduct(n string, v float32) *Product {
	return &Product{
		Name:  n,
		Value: v,
	}
}
