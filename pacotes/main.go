package main

import (
	"fmt"
	"time"

	"gitlab.com/golang-studies/introdution/pacotes/client"
	"gitlab.com/golang-studies/introdution/pacotes/inventory"
)

func main() {
	ltime := time.Now()

	fmt.Println(ltime.Format("2006-01-02T15:04:05"))

	c1 := client.NewPeople("Anakin", "skywalker")
	c1.Age = 38

	c1.Address = append(c1.Address, client.NewAddress("Tatooine", 253))

	fmt.Println("Cliente")
	fmt.Println(c1.Name)
	fmt.Println(c1.Lastname)
	fmt.Println(c1.Age)

	for _, addr := range c1.Address {
		fmt.Println("Endereço:", addr.Description, "Nrº", addr.Number)
	}

	ivt := inventory.NewInventory()
	ivt.AddProduct("livro", 25.80)
	ivt.AddProduct("caderno", 7.80)

	fmt.Println("Inventario")

	for _, pdt := range ivt.Products {
		fmt.Println("Produto:", pdt.Name, "Valor", pdt.Value)
	}
}
