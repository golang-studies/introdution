package client

type People struct {
	Name     string
	Lastname string
	Age      int
	Address  []*Address
}

func NewPeople(name, lName string) *People {
	return &People{
		Name:     name,
		Lastname: lName,
		Age:      0,
		Address:  make([]*Address, 0),
	}
}
