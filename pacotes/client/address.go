package client

type Address struct {
	Description string
	Number      int
}

func NewAddress(dc string, nb int) *Address {
	return &Address{
		Description: dc,
		Number:      nb,
	}
}
